<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class QRCodeTest extends TestCase
{
    public function provider()
    {
        foreach (['Lorem ipsum', '明けましておめでとう！', 'https://gitlab.com', '+336600000', '12345678'] as $text) {
            foreach (range(2, 10) as $matrixPointSize) {
                foreach (['L', 'M', 'Q', 'H'] as $errorCorrectionLevel) {
                    yield [$text, $errorCorrectionLevel, $matrixPointSize];
                }
            }
        }
    }

    /**
     * @dataProvider provider
     */
    public function testPngCanBeDecodedByZbar($text, $errorCorrectionLevel, $matrixPointSize): void
    {
        $filename = '/tmp/check'.rand(1, 1000000).'.png';

        $config = new Config([
            'level' => $errorCorrectionLevel,
            'size' => $matrixPointSize,
            'margin' => 2,
            'back_color' => 0xFFFFFF,
            'fore_color' => 0x000000,
            'filename' => $filename,
        ]);

        $enc = new QRencode($config);
        $image = new QRimage($config);

        $image->png($enc->encode($text));

        $result = exec("zbarimg -q --raw $filename");

        $this->assertEquals($result, $text);

        unlink($filename);
    }

    /**
     * @dataProvider provider
     */
    public function testJpegCanBeDecodedByZbar($text, $errorCorrectionLevel, $matrixPointSize): void
    {
        $filename = '/tmp/check'.rand(1, 1000000).'.png';

        $config = new Config([
            'level' => $errorCorrectionLevel,
            'size' => $matrixPointSize,
            'margin' => 2,
            'back_color' => 0xFFFFFF,
            'fore_color' => 0x000000,
            'filename' => $filename,
        ]);

        $enc = new QRencode($config);
        $image = new QRimage($config);

        $image->jpg($enc->encode($text));

        $result = exec("zbarimg -q --raw $filename");

        $this->assertEquals($result, $text);

        unlink($filename);
    }

}
