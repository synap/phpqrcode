<?php
class MaskGenerator
{
    public function maskNo($maskNo, $x, $y) : int
    {
        switch ($maskNo){
            case 0 : return ($x + $y) & 1;
            case 1 : return $y & 1;
            case 2 : return $x % 3;
            case 3 : return ($x + $y) % 3;
            case 4 : return (((int)($y / 2)) + ((int)($x / 3))) & 1;
            case 5 : return (($x * $y) & 1) + ($x * $y) % 3;
            case 6 : return ((($x * $y) & 1) + ($x * $y) % 3) & 1;
            case 7 : return ((($x * $y) % 3) + (($x + $y) & 1)) & 1;
        }
    }

    private function generateMaskNo($maskNo, $width, $frame)
    {
        $matrix = new Matrix($width);

        for ($y=0; $y<$width; $y++) {
            for ($x=0; $x<$width; $x++) {
                if (!(ord($frame[$y][$x]) & 0x80)) {
                    $matrix->setBit($y, $x, !$this->maskNo($maskNo, $x, $y));
                }
            }
        }

        return $matrix;
    }

    public function makeMaskNo($maskNo, $width, $s, &$d, $maskGenOnly = false)
    {
        $b = 0;
        $bitMask = $this->generateMaskNo($maskNo, $width, $s);

        if ($maskGenOnly) {
            return;
        }

        $d = $s;

        $iterator = new MatrixVisitor($bitMask);

        foreach ($iterator as $cell)
        {
            $y = $cell['x'];
            $x = $cell['y'];
            $bit = (int)$cell['value'];
            if ($bit == 1) {
                $d[$y][$x] = chr(ord($s[$y][$x]) ^ 1);
            }
            $b += (int)(ord($d[$y][$x]) & 1);
        }

        return $b;
    }
}
