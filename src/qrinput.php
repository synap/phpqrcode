<?php
class QRinput
{
    const QR_ECLEVEL_L = 0;
    const QRSPEC_VERSION_MAX = 40;
    const QR_ECLEVEL_H = 3;
    const QR_MODE_NUM = 0;
    const QR_MODE_AN = 1;
    const QR_MODE_KANJI = 3;
    const QR_MODE_8 = 2;
    public $items;
    private $version;
    private $level;

    //----------------------------------------------------------------------
    public function __construct($version = 0, $level = self::QR_ECLEVEL_L)
    {
        if ($version < 0 || $version > self::QRSPEC_VERSION_MAX || $level > self::QR_ECLEVEL_H) {
            throw new Exception('Invalid version no');
            return null;
        }

        $this->version = $version;
        $this->level = $level;
    }

    public function getVersion() : int
    {
        return $this->version;
    }

    //----------------------------------------------------------------------
    public function setVersion(int $version) : void
    {
        if ($version > QRspec::VERSION_MAX) {

            throw new Exception("Version can't be greater than " . QRspec::VERSION_MAX);
        }

        $this->version = $version;
    }

    //----------------------------------------------------------------------
    public function getErrorCorrectionLevel() : int
    {
        return $this->level;
    }

    public function appendEntry(QRinputItem $entry) : void
    {
        $this->items[] = $entry;
    }

    public function append($mode, $size, $data) : void
    {
        $entry = new QRinputItem($mode, $size, $data);

        $this->appendEntry($entry);
    }

    /**
     * Alphanumeric table
     */
    public static $anTable = [
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43,
         0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 44, -1, -1, -1, -1, -1,
        -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
        25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
    ];

    //----------------------------------------------------------------------
    public static function lookAnTable($c)
    {
        return (($c > 127)?-1:self::$anTable[$c]);
    }




    /***********************************************************************
     * Validation
     **********************************************************************/

    public static function check($mode, $size, $data)
    {
        if ($size <= 0) {
            return false;
        }

        switch ($mode) {
            case self::QR_MODE_NUM:
                $modeObject = new \Mode\Digital();
                break;

            case self::QR_MODE_AN:
                $modeObject = new \Mode\Alpha();
                break;

            case self::QR_MODE_KANJI:
                $modeObject = new \Mode\Kanji();
                break;

            case self::QR_MODE_8:
                $modeObject = new \Mode\Byte();
                break;

            case QR_MODE_STRUCTURE:
                return true;

            default:
                return false;
        }

        return $modeObject->check($size, join($data));

    }

    public function estimateVersion()
    {
        $version = 0;
        $prev = 0;
        do {
            $prev = $version;
            $bits = array_reduce($this->items, function($carry, $item) use ($prev) {
                return $carry + $item->estimateBitStreamSizeOfEntry($prev);
            }, 0);

            $version = QRspec::getMinimumVersion((int)(($bits + 7) / 8), $this->level);
            if ($version < 0) {
                return -1;
            }
        } while ($version > $prev);

        return $version;
    }

    public function createBitStream()
    {
        $total = 0;

        foreach ($this->items as $item) {
            $bits = $item->encodeBitStream($this->version);

            if ($bits < 0) {
                return -1;
            }

            $total += $bits;
        }

        return $total;
    }

    private function convertData()
    {
        $ver = $this->estimateVersion();
        if ($ver > $this->getVersion()) {
            $this->setVersion($ver);
        }

        for (;;) {
            $bits = $this->createBitStream();

            if ($bits < 0) {
                return -1;
            }

            $ver = QRspec::getMinimumVersion((int)(($bits + 7) / 8), $this->level);
            if ($ver < 0) {
                throw new Exception('WRONG VERSION');
                return -1;
            } elseif ($ver > $this->getVersion()) {
                $this->setVersion($ver);
            } else {
                break;
            }
        }

        return 0;
    }

    public function appendPaddingBit(&$bstream)
    {
        $bits = $bstream->size();
        $maxwords = QRspec::getDataLength($this->version, $this->level);
        $maxbits = $maxwords * 8;

        if ($maxbits == $bits) {
            return 0;
        }

        if ($maxbits - $bits < 5) {
            return $bstream->append(QRbitstream::newFromNum($maxbits - $bits, 0));
        }

        $bits += 4;
        $words = (int)(($bits + 7) / 8);

        $padding = new QRbitstream();
        $ret = $padding->append(QRbitstream::newFromNum($words * 8 - $bits + 4, 0));

        $padlen = $maxwords - $words;

        if ($padlen > 0) {
            $padbuf = array();
            for ($i=0; $i<$padlen; $i++) {
                $padbuf[$i] = ($i&1)?0x11:0xec;
            }

            $ret = $padding->append(QRbitstream::newFromBytes($padlen, $padbuf));
        }

        $ret = $bstream->append($padding);

        return $ret;
    }

    /**
     * Returns the bitstream corresponding to content chunks
     */
    public function getBitStream() : QRbitstream
    {
        if ($this->convertData() < 0) {
            return null;
        }

        $bstream = array_reduce(
            $this->items,
            function($bstream, $item){
                return $bstream->append($item->bstream);
            },
            new QRbitstream()
        );

        $ret = $this->appendPaddingBit($bstream);


        return $bstream;
    }
}
