<?php


    define('N1', 3);
    define('N2', 3);
    define('N3', 40);
    define('N4', 10);

class QRmask
{
    const QR_FIND_FROM_RANDOM = 2;
    const QRSPEC_WIDTH_MAX = 177;
    public $runLength = array();

    public function __construct()
    {
        $this->runLength = array_fill(0, self::QRSPEC_WIDTH_MAX + 1, 0);
        $this->maskGenerator = new \MaskGenerator();
    }

    private function makeMask($width, $frame, $maskNo, $level)
    {
        $infoWriter = new InformationWriter();
        $masked = array_fill(0, $width, str_repeat("\0", $width));

        $this->maskGenerator->makeMaskNo($maskNo, $width, $frame, $masked);
        $infoWriter->writeFormatInformation($width, $masked, $maskNo, $level);

        return $masked;
    }



    private function evaluateSymbol($width, $frame)
    {
        $head = 0;
        $demerit = 0;
        $n1n3Calculator = new N1N3Calculator();

        for ($y=0; $y<$width; $y++) {
            $head = 0;
            $this->runLength[0] = 1;

            $frameY = $frame[$y];

            if ($y>0) {
                $frameYM = $frame[$y-1];
            }

            for ($x=0; $x<$width; $x++) {
                if (($x > 0) && ($y > 0)) {
                    $b22 = ord($frameY[$x]) & ord($frameY[$x-1]) & ord($frameYM[$x]) & ord($frameYM[$x-1]);
                    $w22 = ord($frameY[$x]) | ord($frameY[$x-1]) | ord($frameYM[$x]) | ord($frameYM[$x-1]);

                    if (($b22 | ($w22 ^ 1))&1) {
                        $demerit += N2;
                    }
                }
                if (($x == 0) && (ord($frameY[$x]) & 1)) {
                    $this->runLength[0] = -1;
                    $head = 1;
                    $this->runLength[$head] = 1;
                } elseif ($x > 0) {
                    if ((ord($frameY[$x]) ^ ord($frameY[$x-1])) & 1) {
                        $head++;
                        $this->runLength[$head] = 1;
                    } else {
                        $this->runLength[$head]++;
                    }
                }
            }

            $demerit += $n1n3Calculator->calcN1N3($this->runLength, $head+1);
        }

        for ($x=0; $x<$width; $x++) {
            $head = 0;
            $this->runLength[0] = 1;

            for ($y=0; $y<$width; $y++) {
                if ($y == 0 && (ord($frame[$y][$x]) & 1)) {
                    $this->runLength[0] = -1;
                    $head = 1;
                    $this->runLength[$head] = 1;
                } elseif ($y > 0) {
                    if ((ord($frame[$y][$x]) ^ ord($frame[$y-1][$x])) & 1) {
                        $head++;
                        $this->runLength[$head] = 1;
                    } else {
                        $this->runLength[$head]++;
                    }
                }
            }
            $demerit += $n1n3Calculator->calcN1N3($this->runLength, $head+1);
        }

        return $demerit;
    }

    public function mask($width, $frame, $level)
    {
        $minDemerit = PHP_INT_MAX;
        $bestMaskNum = 0;
        $bestMask = [];

        $checked_masks = range(0,7);

        if (self::QR_FIND_FROM_RANDOM !== false) {
            $howManuOut = 8-(self::QR_FIND_FROM_RANDOM % 9);
            for ($i = 0; $i <  $howManuOut; $i++) {
                $remPos = rand(0, count($checked_masks)-1);
                unset($checked_masks[$remPos]);
                $checked_masks = array_values($checked_masks);
            }
        }

        $bestMask = $frame;
        $infoWriter = new InformationWriter();
        foreach ($checked_masks as $i) {
            $mask = array_fill(0, $width, str_repeat("\0", $width));

            $demerit = 0;
            $blacks = 0;
            $blacks  = $this->maskGenerator->makeMaskNo($i, $width, $frame, $mask);
            $blacks += $infoWriter->writeFormatInformation($width, $mask, $i, $level);
            $blacks  = (int)(100 * $blacks / ($width * $width));
            $demerit = (int)((int)(abs($blacks - 50) / 5) * N4);
            $demerit += $this->evaluateSymbol($width, $mask);

            if ($demerit < $minDemerit) {
                $minDemerit = $demerit;
                $bestMask = $mask;
                $bestMaskNum = $i;
            }
        }

        return $bestMask;
    }
}
