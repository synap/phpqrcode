<?php
class Encoder
{
    const QR_MODE_8 = 2;
    const QR_MODE_KANJI = 3;

    private $code;

    public function __construct(QRcode $code)
    {
        $this->code = $code;
    }

    public function encode($intext, $version, $level, $hint, $casesensitive)
    {

        if (!in_array($hint, [self::QR_MODE_8,self::QR_MODE_KANJI])) {
            throw new Exception('bad hint');
            return null;
        }

        $input = new QRinput($version, $level);

        //QRsplit::splitStringToQRinput($intext, $input, $hint, $casesensitive);


        $split = new QRsplit($intext, $input, $hint);

        if (!$casesensitive) {
            $split->toUpper();
        }

        $split->splitString();

        return $this->code->encodeInput($input);

    }
}
