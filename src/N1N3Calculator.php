<?php

class N1N3Calculator
{
    const N1 = 3;
    const N2 = 3;
    const N3 = 40;
    const N4 = 10;

  public function calcN1N3($runLength, $length)
  {
      $demerit = 0;

      for ($i=0; $i<$length; $i++) {
          if ($runLength[$i] >= 5) {
              $demerit += (self::N1 + ($runLength[$i] - 5));
          }
          if ($i & 1) {
              if (($i >= 3) && ($i < ($length-2)) && ($runLength[$i] % 3 == 0)) {
                  $fact = (int)($runLength[$i] / 3);
                  if (($runLength[$i-2] == $fact) &&
                     ($runLength[$i-1] == $fact) &&
                     ($runLength[$i+1] == $fact) &&
                     ($runLength[$i+2] == $fact)) {


                      if (($runLength[$i-3] < 0) || ($runLength[$i-3] >= (4 * $fact))) {
                          $demerit += self::N3;
                      } elseif ((($i+3) >= $length) || ($runLength[$i+3] >= (4 * $fact))) {
                          $demerit += self::N3;
                      }


                  }
              }
          }
      }
      return $demerit;
  }
}
