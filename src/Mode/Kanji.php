<?php

namespace Mode;

class Kanji
{
    const INDICATOR = '1000';

    public function estimate($size)
    {
        return (int)(($size / 2) * 13);
    }

    public function check($size, $data)
    {
        if ($size & 1) {
            return false;
        }

        for ($i=0; $i<$size; $i+=2) {
            $val = (ord($data[$i]) << 8) | ord($data[$i+1]);
            if ($val < 0x8140
            || ($val > 0x9ffc && $val < 0xe040)
            || $val > 0xebbf) {
                return false;
            }
        }

        return true;
    }

    public function encode(int $version, $size, $data)
    {
        $bs = new \QRbitstream();

        $bs->appendNum(4, 0x8);
        $bs->appendNum(\QRspec::lengthIndicator(\QRspec::QR_MODE_KANJI, $version), (int)($size / 2));

        for ($i=0; $i<$size; $i+=2) {
            $val = (ord($data[$i]) << 8) | ord($data[$i+1]);
            if ($val <= 0x9ffc) {
                $val -= 0x8140;
            } else {
                $val -= 0xc140;
            }

            $h = ($val >> 8) * 0xc0;
            $val = ($val & 0xff) + $h;

            $bs->appendNum(13, $val);
        }

        return $bs;
    }

    public function length($mode, $version, $bits, $payload)
    {
        return (int)(($payload / 13) * 2);
    }
}
