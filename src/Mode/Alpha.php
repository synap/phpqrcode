<?php

namespace Mode;
use QRbitstream;

class Alpha
{
    const INDICATOR = '0010';

    public function estimate($size) : int
    {
        $bits = (int)($size / 2) * 11;

        return ($size & 1) ? $bits + 6 : $bits;
    }

    public function check($size, string $data) : bool
    {
        return (bool) preg_match('/^[\d+A-Z\ \$\*\+\-\.\/\:]+$/', $data);
    }

    public function encode(int $version, $size, $data)
    {
        $words = (int)($size / 2);
        $bs = new QRbitstream();

        $bs->append(QRbitstream::newFromNum(4, 0x02));
        $bs->append(QRbitstream::newFromNum(\QRspec::lengthIndicator(\QRspec::QR_MODE_AN, $version), $size));

        for ($i=0; $i<$words; $i++) {
            $val  = (int)\QRinput::lookAnTable(ord($data[$i*2  ])) * 45;
            $val += (int)\QRinput::lookAnTable(ord($data[$i*2+1]));

            $bs->append(QRbitstream::newFromNum(11, $val));
        }

        if ($size & 1) {
            $val = \QRinput::lookAnTable(ord($data[$words * 2]));
            $bs->append(QRbitstream::newFromNum(6, $val));
        }

        return $bs;
    }

    public function length($mode, $version, $bits, $payload)
    {
        $chunks = (int)($payload / 11);
        $remain = $payload - $chunks * 11;
        $size = $chunks * 2;

        if ($remain >= 6) {
            $size++;
        }

        return $size;
    }
}
