<?php

namespace Mode;
use QRbitstream;
class Digital
{
    const INDICATOR = '0001';

    public function estimate($size)
    {
        $w = (int)$size / 3;
        $bits = $w * 10;

        switch ($size - $w * 3) {
            case 1:
                $bits += 4;
                break;
            case 2:
                $bits += 7;
                break;
        }

        return $bits;
    }

    public function check($size, string $data)
    {
        return (bool) preg_match('/^\d+$/', $data);
    }

/*    public function encode($content)
    {
            $content = str_split($content, 3);
            $content = array_map('decbin', $content);
            $content = join($content);

        return $content;
    }*/

    /**
     * @see https://www.thonky.com/qr-code-tutorial/numeric-mode-encoding
     */
    public function encode(int $version, $size, $data)
    {
        $words = (int)($size / 3);
        $bs = new \QRbitstream();

        $val = 0x1;
        $bs->append(QRbitstream::newFromNum(4, $val));
        $bs->append(QRbitstream::newFromNum(\QRspec::lengthIndicator(\QRspec::QR_MODE_NUM, $version), $size));

        for ($i=0; $i<$words; $i++) {
            $val  = (ord($data[$i*3  ]) - ord('0')) * 100;
            $val += (ord($data[$i*3+1]) - ord('0')) * 10;
            $val += (ord($data[$i*3+2]) - ord('0'));
            $bs->append(QRbitstream::newFromNum(10, $val));
        }

        if ($size - $words * 3 == 1) {
            $val = ord($data[$words*3]) - ord('0');
            $bs->append(QRbitstream::newFromNum(4, $val));
        } elseif ($size - $words * 3 == 2) {
            $val  = (ord($data[$words*3  ]) - ord('0')) * 10;
            $val += (ord($data[$words*3+1]) - ord('0'));
            $bs->append(QRbitstream::newFromNum(7, $val));
        }

        return $bs;
    }

    public function length($mode, $version, $bits, $payload)
    {
        $chunks = (int)($payload / 10);
        $remain = $payload - $chunks * 10;
        $size = $chunks * 3;
        if ($remain >= 7) {
            $size += 2;
        } elseif ($remain >= 4) {
            $size += 1;
        }

        return $size;
    }
}
