<?php

namespace Mode;

class Byte
{
    const INDICATOR = '0100';

    public function estimate($size)
    {
        return $size * 8;
    }

    public function check($size, string $data)
    {
        return strlen($data) === $size;
    }

    /**
     * @see https://www.thonky.com/qr-code-tutorial/byte-mode-encoding
     */
    public function encode($version, $size, $data)
    {
        $bs = new \QRbitstream();

        $bs->appendNum(4, 0x4);
        $bs->appendNum(\QRspec::lengthIndicator(\QRspec::QR_MODE_8, $version), $size);

        for ($i=0; $i<$size; $i++) {
            $bs->appendNum(8, ord($data[$i]));
        }

        return $bs;
    }

    public function length($mode, $version, $bits, $payload)
    {
        return (int)($payload / 8);
    }
}
