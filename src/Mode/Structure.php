<?php

namespace Mode;

class Structure
{
    const INDICATOR = '0010';

    public function encode()
    {
        $bs =  new QRbitstream();

        $bs->appendNum(4, 0x03);
        $bs->appendNum(4, ord($this->data[1]) - 1);
        $bs->appendNum(4, ord($this->data[0]) - 1);
        $bs->appendNum(8, ord($this->data[2]));

        return $bs;
    }

    public function length($mode, $version, $bits, $payload)
    {
        return (int)($payload / 8);
    }
}
