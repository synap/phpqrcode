<?php

class QRbitstream
{
    private $data = [];

    public function size() : int
    {
        return count($this->data);
    }

    private function allocate(int $setLength) : void
    {
        $this->data = array_fill(0, $setLength, 0);
    }

    /**
     * certainement remplaçable par pack / unpack
     */
    public static function newFromNum($bits, $num) : QRbitstream
    {
        $bstream = new QRbitstream();
        $bstream->data = str_split(sprintf( "%0{$bits}d", decbin($num)));

        return $bstream;
    }

    public static function newFromBytes($size, $data) : QRbitstream
    {
        $bstream = new QRbitstream();
        $bstream->allocate($size * 8);
        $p=0;

        for ($i=0; $i<$size; $i++) {
            $mask = 0x80;
            for ($j=0; $j<8; $j++) {
                $bstream->data[$p] = ($data[$i] & $mask) ? 1 : 0;
                $p++;
                $mask = $mask >> 1;
            }
        }

        return $bstream;
    }

    public function append(QRbitstream $arg) : QRbitstream
    {
        $this->data = array_values(array_merge($this->data, $arg->data));
        return $this;
    }

    public function appendNum(int $bits, $num)
    {
        return $this->append(QRbitstream::newFromNum($bits, $num));
    }

    public function appendBytes(int $size, array $data)
    {
        return $this->append(QRbitstream::newFromBytes($size, $data));
    }

    public function toByte()
    {
        $size = $this->size();

        if ($size == 0) {
            return array();
        }

        $data = array_fill(0, (int)(($size + 7) / 8), 0);
        $bytes = (int)($size / 8);

        $p = 0;

        for ($i=0; $i<$bytes; $i++) {
            $v = 0;
            for ($j=0; $j<8; $j++) {
                $v = $v << 1;
                $v |= $this->data[$p];
                $p++;
            }
            $data[$i] = $v;
        }

        if ($size & 7) {
            $v = 0;
            for ($j=0; $j<($size & 7); $j++) {
                $v = $v << 1;
                $v |= $this->data[$p];
                $p++;
            }
            $data[$bytes] = $v;
        }

        return $data;
    }
}
