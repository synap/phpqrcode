<?php

class QRtools
{
    public static function binarize($frame)
    {
        $len = count($frame);

        return array_map(function($line) use ($len) {
            $a = '';
            for ($i=0; $i<$len; $i++) {
                //$frameLine[$i] = decbin(ord($frameLine[$i]));
                $a .= (ord($line[$i])&1) ? '1' : '0';
            }
            return $a;
        }, $frame);
    }

}
