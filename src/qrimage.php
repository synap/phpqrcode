<?php

class QRimage
{
    private $stack = [];
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }
    /**
     * Generate a  PNG file from a frame object
     */
    public function png($frame)
    {
        ImagePng(
            $this->image($frame, $this->config->getSize(), $this->config->getMargin(), $this->config->getBackColor(), $this->config->getForeColor()),
            $this->config->getFilename()
        );
    }

    /**
     * Generate a JPG file from a frame object
     */
    public function jpg($frame, $q = 85)
    {
        ImageJpeg(
            $this->image($frame, $this->config->getSize(), $this->config->getMargin(), $this->config->getBackColor(), $this->config->getForeColor()),
            $this->config->getFilename(),
            $q
        );
    }

    /**
     * Draw an image from a given frame, by using GD extension API.
     */
    private function image($frame, $pixelPerPoint = 4, $outerFrame = 4, $back_color = 0xFFFFFF, $fore_color = 0x000000)
    {
        $h = count($frame);
        $w = strlen($frame[0]);

        $imgW = $w + 2*$outerFrame;
        $imgH = $h + 2*$outerFrame;

        $base_image = ImageCreate($imgW, $imgH);

        $this->stack[] = &$base_image;

        // convert a hexadecimal color code into decimal eps format (green = 0 1 0, blue = 0 0 1, ...)
        $r1 = round((($fore_color & 0xFF0000) >> 16), 5);
        $b1 = round((($fore_color & 0x00FF00) >> 8), 5);
        $g1 = round(($fore_color & 0x0000FF), 5);

        // convert a hexadecimal color code into decimal eps format (green = 0 1 0, blue = 0 0 1, ...)
        $r2 = round((($back_color & 0xFF0000) >> 16), 5);
        $b2 = round((($back_color & 0x00FF00) >> 8), 5);
        $g2 = round(($back_color & 0x0000FF), 5);



        $col[0] = ImageColorAllocate($base_image, $r2, $b2, $g2);
        $col[1] = ImageColorAllocate($base_image, $r1, $b1, $g1);

        imagefill($base_image, 0, 0, $col[0]);


        $matrix = new Matrix($h);
        $matrix->setGrid($frame);

        $iterator = new MatrixVisitor($matrix);
        foreach ($iterator as $pixel)
        {
            //list($x,$y,$value) = $pixel;

            if ($pixel['value'] == '1') {
                ImageSetPixel($base_image, $pixel['y']+$outerFrame, $pixel['x']+$outerFrame, $col[1]);
            }

        }
/*
        for ($y=0; $y<$h; $y++) {
            for ($x=0; $x<$w; $x++) {
                if ($frame[$y][$x] == '1') {
                    ImageSetPixel($base_image, $x+$outerFrame, $y+$outerFrame, $col[1]);
                }
            }
        }
*/
        $target_image =ImageCreate($imgW * $pixelPerPoint, $imgH * $pixelPerPoint);

        $this->stack[] = &$target_image;

        ImageCopyResized($target_image, $base_image, 0, 0, 0, 0, $imgW * $pixelPerPoint, $imgH * $pixelPerPoint, $imgW, $imgH);

        return $target_image;
    }

    public function __destruct()
    {
        foreach ($this->stack as $image)
        {
            imagedestroy($image);
        }
    }
}
