<?php

define('STRUCTURE_HEADER_BITS', 20);
define('MAX_STRUCTURED_SYMBOLS', 16);

class QRinputItem
{
    const QR_MODE_NUM = 0;
    const QR_MODE_AN = 1;
    const QR_MODE_KANJI = 3;
    const QR_MODE_8 = 2;
    public $mode;
    public $size;
    public $data;
    public $bstream;

    public function __construct($mode, $size, $data, $bstream = null)
    {
        $setData = array_slice($data, 0, $size);

        if (count($setData) < $size) {
            $setData = array_merge($setData, array_fill(0, $size-count($setData), 0));
        }

        if (!QRinput::check($mode, $size, $setData)) {
            throw new Exception('Error m:'.$mode.',s:'.$size.',d:'.join(',', $setData));
            return null;
        }

        $this->mode = $mode;
        $this->size = $size;
        $this->data = $setData;
        $this->bstream = $bstream;
    }

    public function estimateBitStreamSizeOfEntry($version)
    {
        $bits = 0;

        if ($version == 0) {
            $version = 1;
        }

        switch ($this->mode) {
            case self::QR_MODE_NUM:
            $m = new Mode\Digital();
            $bits = $m->estimate($this->size);
                break;
            case self::QR_MODE_AN:
                    $m = new Mode\Alpha();
                    $bits = $m->estimate($this->size);
                break;
            case self::QR_MODE_8:
                $m = new Mode\Byte();
                $bits = $m->estimate($this->size);
                break;
            case self::QR_MODE_KANJI:
                $m = new Mode\Kanji();
                $bits = $m->estimate($this->size);
                break;
            case QR_MODE_STRUCTURE:
                return STRUCTURE_HEADER_BITS;
            default:
                return 0;
        }

        $l = QRspec::lengthIndicator($this->mode, $version);
        $m = 1 << $l;
        $num = (int)(($this->size + $m - 1) / $m);

        $bits += $num * (4 + $l);

        return $bits;
    }

    //----------------------------------------------------------------------
    public function encodeBitStream($version)
    {
        unset($this->bstream);
        $words = QRspec::maximumWords($this->mode, $version);

        if ($this->size > $words) {
            $st1 = new QRinputItem($this->mode, $words, $this->data);
            $st2 = new QRinputItem($this->mode, $this->size - $words, array_slice($this->data, $words));

            $st1->encodeBitStream($version);
            $st2->encodeBitStream($version);

            $this->bstream =
                (new QRbitstream())
                    ->append($st1->bstream)
                    ->append($st2->bstream)
            ;

            unset($st1);
            unset($st2);
        } else {

            switch ($this->mode) {
                case self::QR_MODE_NUM: $modeObject = new \Mode\Digital(); break;
                case self::QR_MODE_AN: $modeObject = new \Mode\Alpha(); break;
                case self::QR_MODE_8: $modeObject = new \Mode\Byte(); break;
                case self::QR_MODE_KANJI: $modeObject = new \Mode\Kanji(); break;
                case self::QR_MODE_STRUCTURE: $modeObject = new \Mode\Structure(); break;
            }
            $this->bstream = $modeObject->encode($version, $this->size, $this->data);
        }

        return $this->bstream->size();
    }
};
