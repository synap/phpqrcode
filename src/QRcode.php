<?php
class QRcode
{
    const QR_MODE_8 = 2;
    const QR_ECLEVEL_L = 0;
    const QRSPEC_VERSION_MAX = 40;
    const QR_ECLEVEL_H = 3;
    const QR_FIND_BEST_MASK = true;
    public $version;
    public $width;
    public $data;


    private $specs;

    public function __construct(QRspec $specs)
    {
        $this->specs = $specs;
    }

    //----------------------------------------------------------------------
    public function encodeMask(QRinput $input, $mask)
    {
        $raw = new QRrawcode($input);

        $version = $raw->version;

        /**
         * mettre la version dans une factory qui donne un QRSpect dont le getWidth() ou le newFrame()
         * n'a pas besoin de passer la version en parametre.
         */
        $width = $this->specs->getWidth($version);
        $frame = $this->specs->newFrame($version);

        $filler = new FrameFiller($width, $frame);
        if (is_null($filler)) {
            return null;
        }

        // inteleaved data and ecc codes
        for ($i=0; $i<$raw->dataLength + $raw->eccLength; $i++) {
            $code = $raw->getCode();
            $bit = 0x80;
            for ($j=0; $j<8; $j++) {
                $addr = $filler->next();
                $filler->setFrameAt($addr, 0x02 | (($bit & $code) != 0));
                $bit = $bit >> 1;
            }
        }

        unset($raw);

        // remainder bits
        $j = $this->specs->getRemainder($version);
        for ($i=0; $i<$j; $i++) {
            $addr = $filler->next();
            $filler->setFrameAt($addr, 0x02);
        }

        $frame = $filler->frame;
        unset($filler);


        // masking
        $maskObj = new QRmask();
        if ($mask < 0) {
            if (self::QR_FIND_BEST_MASK) {
                $masked = $maskObj->mask($width, $frame, $input->getErrorCorrectionLevel());
            } else {
                $masked = $maskObj->makeMask($width, $frame, (intval(QR_DEFAULT_MASK) % 8), $input->getErrorCorrectionLevel());
            }
        } else {
            $masked = $maskObj->makeMask($width, $frame, $mask, $input->getErrorCorrectionLevel());
        }

        if ($masked == null) {
            return null;
        }

        $this->version = $version;
        $this->width = $width;
        $this->data = $masked;

        return $this;
    }

    //----------------------------------------------------------------------
    public function encodeInput(QRinput $input)
    {
        return $this->encodeMask($input, -1);
    }

}
