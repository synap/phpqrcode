<?php
class Parser
{
    private $current;

    public function __construct(array $modes)
    {

    }

    public function parse(string $str)
    {
        for($i=0; $i< strlen($str); $i++)
        {
            foreach($this->modes as $mode)
            {
                $checked = $mode->check($str[$i]);
                $previous = $this->states[$mode::IDENTIFICATOR];

                if ($previous && !$checked) {
                    $this->emit('end:'.$mode::IDENTIFICATOR);
                } elseif (!$previous && $checked) {
                    $this->emit('start:'.$mode::IDENTIFICATOR);
                }
            }
        }
    }

    public function emit($signal)
    {

    }
}
