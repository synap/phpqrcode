<?php

namespace Writer;

class EPS
{
    //----------------------------------------------------------------------
    private static function write($frame, $pixelPerPoint = 4, $outerFrame = 4, $back_color = 0xFFFFFF, $fore_color = 0x000000)
    {
        $h = count($frame);
        $w = strlen($frame[0]);

        $imgW = $w + 2*$outerFrame;
        $imgH = $h + 2*$outerFrame;

        // convert a hexadecimal color code into decimal eps format (green = 0 1 0, blue = 0 0 1, ...)
        $r = round((($fore_color & 0xFF0000) >> 16) / 255, 5);
        $b = round((($fore_color & 0x00FF00) >> 8) / 255, 5);
        $g = round(($fore_color & 0x0000FF) / 255, 5);
        $fore_color = $r.' '.$b.' '.$g;

        // convert a hexadecimal color code into decimal eps format (green = 0 1 0, blue = 0 0 1, ...)
        $r = round((($back_color & 0xFF0000) >> 16) / 255, 5);
        $b = round((($back_color & 0x00FF00) >> 8) / 255, 5);
        $g = round(($back_color & 0x0000FF) / 255, 5);
        $back_color = $r.' '.$b.' '.$g;

        $output =
        '%!PS-Adobe EPSF-3.0'."\n".
        '%%Creator: Zend_Matrixcode_Qrcode'."\n".
        '%%Title: QRcode'."\n".
        '%%CreationDate: '.date('Y-m-d')."\n".
        '%%DocumentData: Clean7Bit'."\n".
        '%%LanguageLevel: 2'."\n".
        '%%Pages: 1'."\n".
        '%%BoundingBox: 0 0 '.$imgW * $pixelPerPoint.' '.$imgH * $pixelPerPoint."\n";

        // set the scale
        $output .= $pixelPerPoint.' '.$pixelPerPoint.' scale'."\n";
        // position the center of the coordinate system

        $output .= $outerFrame.' '.$outerFrame.' translate'."\n";




        // redefine the 'rectfill' operator to shorten the syntax
        $output .= '/F { rectfill } def'."\n";

        // set the symbol color
        $output .= $back_color.' setrgbcolor'."\n";
        $output .= '-'.$outerFrame.' -'.$outerFrame.' '.($w + 2*$outerFrame).' '.($h + 2*$outerFrame).' F'."\n";


        // set the symbol color
        $output .= $fore_color.' setrgbcolor'."\n";

        // Convert the matrix into pixels

        for ($i=0; $i<$h; $i++) {
            for ($j=0; $j<$w; $j++) {
                if ($frame[$i][$j] == '1') {
                    $y = $h - 1 - $i;
                    $x = $j;
                    $output .= $x.' '.$y.' 1 1 F'."\n";
                }
            }
        }


        $output .='%%EOF';

        return $output;
    }
}
