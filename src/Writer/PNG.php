<?php

namespace Writer;

class PNG
{
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function encode()
    {
        $enc = new QRencode();
        $enc->size = $size;
        $enc->margin = $margin;
        $enc->fore_color = $fore_color;
        $enc->back_color = $back_color;

        $level = strtoupper($level);

        switch ($level.'') {
            case 'L': $enc->level = QRspec::QR_ECLEVEL_L; break;
            case 'M': $enc->level = QRspec::QR_ECLEVEL_M; break;
            case 'Q': $enc->level = QRspec::QR_ECLEVEL_Q; break;
            case 'H': $enc->level = QRspec::QR_ECLEVEL_H; break;
        }

        $tab = $enc->encode($intext);

        $image = new QRimage();
        $image->png(
            $tab,
            $outfile,
            $this->config
        );

    }
}
