<?php

namespace Writer;

class SVG
{
    public function write($frame, $pixelPerPoint = 4, $outerFrame = 4, $back_color = 0xFFFFFF, $fore_color = 0x000000)
    {
        $h = count($frame);
        $w = strlen($frame[0]);

        $imgW = $w + 2*$outerFrame;
        $imgH = $h + 2*$outerFrame;

        $output =
        '<?xml version="1.0" encoding="utf-8"?>'."\n".
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">'."\n".
        '<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" xmlns:xlink="http://www.w3.org/1999/xlink" width="'.$imgW * $pixelPerPoint.'" height="'.$imgH * $pixelPerPoint.'" viewBox="0 0 '.$imgW * $pixelPerPoint.' '.$imgH * $pixelPerPoint.'">'."\n".
        '<desc></desc>'."\n";

        if (!empty($back_color)) {
            $backgroundcolor = dechex($back_color);
            $output .= '<rect width="'.$imgW * $pixelPerPoint.'" height="'.$imgH * $pixelPerPoint.'" fill="#'.$backgroundcolor.'" cx="0" cy="0" />'."\n";
        }

        $output .=
        '<defs>'."\n".
        '<rect id="p" width="'.$pixelPerPoint.'" height="'.$pixelPerPoint.'" />'."\n".
        '</defs>'."\n".
        '<g fill="#'.dechex($fore_color).'">'."\n";


        // Convert the matrix into pixels

        for ($i=0; $i<$h; $i++) {
            for ($j=0; $j<$w; $j++) {
                if ($frame[$i][$j] == '1') {
                    $y = ($i + $outerFrame) * $pixelPerPoint;
                    $x = ($j + $outerFrame) * $pixelPerPoint;
                    $output .= '<use x="'.$x.'" y="'.$y.'" xlink:href="#p" />'."\n";
                }
            }
        }
        $output .=
        '</g>'."\n".
        '</svg>';

        return $output;
    }
}
