<?php
/**
 * Matrix class to handle bit grid
 *
 * Instead of working with arrays of arrays, it's better to rely on this
 * abstraction to avoid too many for(){for(){..}}.
 *
 * You should also have a look on MatrixVisitor class which makes traversing
 * more easy.
 */
class Matrix
{
    private $grid;
    private $size;

    public function __construct($size)
    {
        $this->size = $size;
        $this->grid = array_fill(false, $size, array_fill(false, $size, false));
    }

    /**
     * To use an already existing array of arrays, juste set it.
     *
     * @var $grid an already existing array of arrays
     */
    public function setGrid($grid)
    {
        $this->grid = $grid;
    }

    /**
     * Return the size of the matrix.
     *
     * As it's intended to be a "square", the size is the same for width and height
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set a boolean value to specified ooordinates.
     */
    public function setBit($x, $y, bool $val)
    {
        $this->grid[$x][$y] = $val;
    }

    /**
     * Return the boolean value of specified coordinates.
     */
    public function getBit($x, $y) : bool
    {
        return $this->grid[$x][$y];
    }

    /**
     * Should not be usable at the end. This is just an helper to convert
     * the matrix of bits to a matrix of 1 and 0 suring the refactoring.
     */
    public function getGrid()
    {
        for($i=0; $i<pow($this->size,2); $i++) {
            $x = $i%$this->size;

            $y = (int)$i/$this->size;
            $result[$x][$y] = $this->grid[$x][$y] ? 1 : 0;
        }

        return $result;
    }
}
