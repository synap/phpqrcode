<?php

class QRsplit
{
    const QR_MODE_KANJI = 3;
    const QR_MODE_NUL = -1;
    const QR_MODE_NUM = 0;
    const QR_MODE_AN = 1;
    const QR_MODE_8 = 2;

    public $dataStr = '';
    public $input;
    public $modeHint;

    public function __construct($dataStr, $input, $modeHint)
    {
        $this->dataStr  = $dataStr;
        $this->input    = $input;
        $this->modeHint = $modeHint;
    }

    public function identifyMode($pos)
    {
        if ($pos >= strlen($this->dataStr)) {
            return self::QR_MODE_NUL;
        }

        $c = $this->dataStr[$pos];

        $mode = new Mode\Digital();

        if ($mode->check(1, $this->dataStr[$pos]))
        {
            return self::QR_MODE_NUM;

        }

        $mode = new Mode\Alpha();

        if ($mode->check(1, $this->dataStr[$pos]))
        {
            return self::QR_MODE_AN;

        }

        if ($this->modeHint == self::QR_MODE_KANJI) {
            if ($pos+1 < strlen($this->dataStr)) {
                $d = $this->dataStr[$pos+1];
                $word = (ord($c) << 8) | ord($d);
                if (($word >= 0x8140 && $word <= 0x9ffc) || ($word >= 0xe040 && $word <= 0xebbf)) {
                    return self::QR_MODE_KANJI;
                }
            }
        }

        return self::QR_MODE_8;
    }

    public function splitString()
    {
        $alphaEater = new Eater\Alpha();
        $digitalEater = new Eater\Digital();
        $kanjiEater = new Eater\Kanji();
        $byteEater = new Eater\Byte();

        while (strlen($this->dataStr) > 0) {

            $mode = $this->identifyMode(0);

            switch ($mode) {
                case self::QR_MODE_NUM:
                    $eater = $digitalEater;
                    break;

                case self::QR_MODE_AN:
                    $eater = $alphaEater;
                    break;

                case self::QR_MODE_KANJI:
                    if ($hint == self::QR_MODE_KANJI) {
                        $eater = $kanjiEater;
                    } else {
                        $eater = $byteEater;
                    }
                    break;
                default:
                        $eater = $byteEater;
                    break;
            }

            $length = $eater->eat($this->input, $this->dataStr, $this);

            if ($length == 0) {
                return 0;
            }
            if ($length < 0) {
                return -1;
            }

            $this->dataStr = substr($this->dataStr, $length);
        }

        return $this;
    }

    public function toUpper()
    {
        $this->dataStr = mb_strtoupper($this->dataStr);

        return $this;
    }
}
