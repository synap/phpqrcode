<?php
/**
 * Configuration class.
 *
 * Handles default values and check validity of QR code generation options.
 */
class Config
{
    private $params;

    public function __construct($options)
    {
        $default = [
            'level' => QRspec::QR_ECLEVEL_L,
            'size' => 3,
            'margin' => 4,
            'back_color' => 0xFFFFFF,
            'fore_color' => 0x000000,
            'filename' => 'php://stdout'
        ];

        $this->params = array_replace($default, $options);
    }

    public function getFilename()
    {
        return $this->params['filename'];
    }

    public function getErrorCorrectionLevel() : string
    {
        return $this->params['level'];
    }

    public function getSize() : int
    {
        return $this->params['size'];
    }

    public function getMargin()
    {
        return $this->params['margin'];
    }

    public function getForeColor()
    {
        return $this->params['fore_color'];
    }

    public function getBackColor()
    {
        return $this->params['back_color'];
    }
}
