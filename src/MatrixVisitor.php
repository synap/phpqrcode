<?php
/**
 * Iterator for matrix structures
 *
 * Instead of :
 *
 * for($i=0;$i<$size;$i++) {
 *    for($j=0;$j<$size;$j++) {
 *        dosomething($i, $j, $grid[$i][$j]);
 *    }
 * }
 *
 * Just do :
 *
 * foreach ($matrix as $pixel) {
 *   dosomething($pixel['x'], $pixel['y'], $pixel['value']);
 * }
 *
 * Or :
 *
 * foreach ($matrix as $pixel) {
 *   list($x, $y, $val) = $pixel;
 *   dosomething($x, $y, $val);
 * }
 */
class MatrixVisitor implements Iterator
{
    private $matrix;
    private $x;
    private $y;
    private $position;

    public function __construct(Matrix $m)
    {
        $this->matrix = $m;
        $this->position = 0;
    }

    public function current()
    {
        return [
            'x' => $x = (int)($this->position/$this->matrix->getSize()),
            'y' => $y = (int)($this->position%$this->matrix->getSize()),
            'value' => $this->matrix->getBit($x, $y)
        ];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        $this->position += 1;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function valid()
    {
        return $this->position < pow($this->matrix->getSize(), 2);
    }
}
