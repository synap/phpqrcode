<?php

class QRrsItem
{
    public $mm;            // Bits per symbol
    public $nn;            // Symbols per block (= (1<<mm)-1)
    public $alpha_to = []; // log lookup table
    public $index_of = []; // Antilog lookup table
    public $genpoly = [];  // Generator polynomial
    public $nroots;        // Number of generator roots = number of parity symbols
    public $fcr;           // First consecutive root, index form
    public $prim;          // Primitive element, index form
    public $iprim;         // prim-th root of 1, index form
    public $pad;           // Padding bytes in shortened block
    public $gfpoly;

    public function modnn($x)
    {
        while ($x >= $this->nn) {
            $x -= $this->nn;
            $x = ($x >> $this->mm) + ($x & $this->nn);
        }

        return $x;
    }

    public function encode_rs_char($data, &$parity)
    {
        $MM       =& $this->mm;
        $NN       =& $this->nn;
        $ALPHA_TO =& $this->alpha_to;
        $INDEX_OF =& $this->index_of;
        $GENPOLY  =& $this->genpoly;
        $NROOTS   =& $this->nroots;
        $FCR      =& $this->fcr;
        $PRIM     =& $this->prim;
        $IPRIM    =& $this->iprim;
        $PAD      =& $this->pad;
        $A0       =& $NN;

        $parity = array_fill(0, $NROOTS, 0);

        for ($i=0; $i< ($NN-$NROOTS-$PAD); $i++) {
            $feedback = $INDEX_OF[$data[$i] ^ $parity[0]];
            if ($feedback != $A0) {
                // feedback term is non-zero

                // This line is unnecessary when GENPOLY[NROOTS] is unity, as it must
                // always be for the polynomials constructed by init_rs()
                $feedback = $this->modnn($NN - $GENPOLY[$NROOTS] + $feedback);

                for ($j=1; $j<$NROOTS; $j++) {
                    $parity[$j] ^= $ALPHA_TO[$this->modnn($feedback + $GENPOLY[$NROOTS-$j])];
                }
            }

            // Shift
            array_shift($parity);
            if ($feedback != $A0) {
                array_push($parity, $ALPHA_TO[$this->modnn($feedback + $GENPOLY[0])]);
            } else {
                array_push($parity, 0);
            }
        }
    }
}
