<?php
class Encoder8bits
{
    private $code;

    public function __construct(QRCode $code)
    {
        $this->code = $code;
    }

    public function encode($intext, $version, $level, $hint, $casesensitive)
    {

        if (string == null) {
            throw new Exception('empty string!');
            return null;
        }

        $input = new QRinput($version, $level);

        if ($input == null) {
            return null;
        }

        $ret = $input->append($input, self::QR_MODE_8, strlen($string), str_split($string));

        if ($ret < 0) {
            unset($input);
            return null;
        }

        return $this->code->encodeInput($input);
    }
}
