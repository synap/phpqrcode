<?php

namespace Eater;

use QRspec;
use Mode;
use QRsplit;

/**
 * Eater for ShiftJIS japanese chunks
 */
class Kanji
{
    /**
     * legacy string consumer
     */
    public function eat(\QRInput $input, &$dataStr, $spliter)
    {
        $p = 0;

        while ($spliter->identifyMode($p) == QRsplit::QR_MODE_KANJI) {
            $p += 2;
        }

        $ret = $input->append(QRsplit::QR_MODE_KANJI, $p, str_split($dataStr));
        if ($ret < 0) {
            return -1;
        }

        return $run;
    }
}
