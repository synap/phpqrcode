<?php

namespace Eater;

use QRspec;
use QRsplit;

/**
 * Eater for byte chunks
 */
class Byte
{
    /**
     * legacy string consumer
     */
    public function eat(\QRInput $input, &$dataStr, $spliter)
    {
        $la = QRspec::lengthIndicator(QRsplit::QR_MODE_AN, $input->getVersion());
        $ln = QRspec::lengthIndicator(QRsplit::QR_MODE_NUM, $input->getVersion());

        $p = 1;
        $dataStrLen = strlen($dataStr);

        $modeAlpha = new \Mode\Alpha();
        $modeDigital = new \Mode\Digital();
        $modeByte = new \Mode\Byte();

        while ($p < $dataStrLen) {
            $mode = $spliter->identifyMode($p);
            if ($mode == QRsplit::QR_MODE_KANJI) {
                break;
            }
            if ($mode == QRsplit::QR_MODE_NUM) {
                $q = $p;
                while ($modeDigital->check(null, $dataStr[$q])) {
                    $q++;
                }
                $dif = $modeByte->estimate($p) // + 4 + l8
                     + $modeDigital->estimate($q - $p) + 4 + $ln
                     - $modeByte->estimate($q); // - 4 - l8
                if ($dif < 0) {
                    break;
                } else {
                    $p = $q;
                }
            } elseif ($mode == QRsplit::QR_MODE_AN) {
                $q = $p;
                while ($modeAlpha->check(null, $dataStr[$q])) {
                    $q++;
                }
                $dif = $modeByte->estimate($p)  // + 4 + l8
                     + $modeAlpha->estimate($q - $p) + 4 + $la
                     - $modeByte->estimate($q); // - 4 - l8
                if ($dif < 0) {
                    break;
                } else {
                    $p = $q;
                }
            } else {
                $p++;
            }
        }

        $run = $p;
        $ret = $input->append(QRsplit::QR_MODE_8, $run, str_split($dataStr));

        if ($ret < 0) {
            return -1;
        }

        return $run;
    }
}
