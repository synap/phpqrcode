<?php

namespace Eater;

use QRspec;
use QRsplit;
use Mode;

/**
 * Eater for numeric chunks
 */
class Digital
{
    /**
     * legacy string consumer
     */
    public function eat(\QRInput $input, &$dataStr, $spliter)
    {
        $ln = QRspec::lengthIndicator(QRsplit::QR_MODE_NUM, $input->getVersion());

        $p = 0;
        $modeDigital = new Mode\Digital();
        while ($modeDigital->check(null, $dataStr[$p] ?? '')) {
            $p++;
        }

        $run = $p;
        $mode = $spliter->identifyMode($p);

        $modeAlpha = new \Mode\Alpha();
        $modeByte = new \Mode\Byte();

        if ($mode == QRsplit::QR_MODE_8) {
            $dif = $modeDigital->estimate($run) + 4 + $ln
                 + $modeByte->estimate(1)         // + 4 + l8
                 - $modeByte->estimate($run + 1); // - 4 - l8
            if ($dif > 0) {
                $eater = new Eater\Byte();
                return $eater->eat($input, $dataStr, $spliter);
            }
        }
        if ($mode == QRsplit::QR_MODE_AN) {

            $dif = $modeDigital->estimate($run) + 4 + $ln
                 + $modeAlpha->estimate(1)        // + 4 + la
                 - $modeAlpha->estimate($run + 1);// - 4 - la
            if ($dif > 0) {
                $eater = new Eater\Alpha();
                return $eater->eat($input, $dataStr, $spliter);
            }
        }

        $ret = $input->append(QRsplit::QR_MODE_NUM, $run, str_split($dataStr));
        if ($ret < 0) {
            return -1;
        }

        return $run;
    }

}
