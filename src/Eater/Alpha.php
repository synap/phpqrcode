<?php

namespace Eater;

use QRspec;
use Mode;
use QRsplit;

/**
 * Eater for alphanumeric chunks
 */
class Alpha
{
    /**
     * legacy string consumer
     */
    public function eat(\QRInput $input, &$dataStr, $spliter)
    {
        $la = QRspec::lengthIndicator(QRsplit::QR_MODE_AN, $input->getVersion());
        $ln = QRspec::lengthIndicator(QRsplit::QR_MODE_NUM, $input->getVersion());

        $p = 0;

        $modeAlpha = new \Mode\Alpha();
        $modeDigital = new \Mode\Digital();
        $modeByte = new \Mode\Byte();

        while ($modeAlpha->check(null, $dataStr[$p])) {
            if ($modeDigital->check(null, $dataStr[$p])) {
                $q = $p;
                while ($modeDigital->check(null, $dataStr[$q] ?? '')) {
                    $q++;
                }

                $dif = $modeAlpha->estimate($p) // + 4 + la
                     + $modeDigital->estimate($q - $p) + 4 + $ln
                     - $modeAlpha->estimate($q); // - 4 - la

                if ($dif < 0) {
                    break;
                } else {
                    $p = $q;
                }
            } else {
                $p++;
            }
        }

        $run = $p;

        if (!$modeAlpha->check(null, $dataStr[$p])) {
            $dif = $modeAlpha->estimate($run) + 4 + $la
                 + $modeByte->estimate(1) // + 4 + l8
                  - $modeByte->estimate($run + 1); // - 4 - l8
            if ($dif > 0) {
                $eater = new Byte();
                return $eater->eat($input, $dataStr, $spliter);
            }
        }

        $ret = $input->append(QRsplit::QR_MODE_AN, $run, str_split($dataStr));
        if ($ret < 0) {
            return -1;
        }

        return $run;
    }
}
