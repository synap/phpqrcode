<?php

class QRencode
{
    const QR_ECLEVEL_L = 0;
    const QR_ECLEVEL_H = 3;
    const QR_MODE_8 = 2;
    const QR_PNG_MAXIMUM_SIZE = 1024;

    public $casesensitive = true;
    public $eightbit = false;

    public $version = 0;
    public $size = 3;
    public $margin = 4;
    public $back_color = 0xFFFFFF;
    public $fore_color = 0x000000;

    public $structured = 0; // not supported yet

    public $level = self::QR_ECLEVEL_L;
    public $hint = self::QR_MODE_8;

    public function __construct(Config $config)
    {
        $this->size = $config->getSize();
        $this->margin = $config->getMargin();
        $this->fore_color = $config->getForeColor();
        $this->back_color = $config->getBackColor();
        //$this->level = $config->getErrorCorrectionLevel();

        switch ($config->getErrorCorrectionLevel()) {
            case 'L': $this->level = QRspec::QR_ECLEVEL_L; break;
            case 'M': $this->level = QRspec::QR_ECLEVEL_M; break;
            case 'Q': $this->level = QRspec::QR_ECLEVEL_Q; break;
            case 'H': $this->level = QRspec::QR_ECLEVEL_H; break;
        }
    }

    //----------------------------------------------------------------------
    public function encodeRAW($intext, $outfile = false)
    {
        $specs = new QRspec();
        $code = new QRcode($specs);


        if ($this->eightbit) {
            $code->encodeString8bit($intext, $this->version, $this->level);
        } else {
            $code->encodeString($intext, $this->version, $this->level, $this->hint, $this->casesensitive);
        }

        return $code->data;
    }

    public function encode($intext, $outfile = false)
    {
        $specs = new QRspec();
        $code = new QRcode($specs);

        if ($this->eightbit) {
            $encoder = new Encoder8bits($code);
        } else {
            $encoder = new Encoder($code);
        }

        $data = $encoder->encode($intext, $this->version, $this->level, $this->hint, $this->casesensitive);

        return QRtools::binarize($code->data);
    }
}
